# Pipeline-template

Esse projeto armazena as pipelines que serão usadas nos projetos.
## Exemplo de Uso

Na pasta `exemplo` está o modelo que deve ser adicionado nos respectivos projetos das aplicações.
Esse arquivo deverá apontar para a pipeline-template que será usada no projeto.

## Benefícios
Mantendo a pipeline em um único projeto, conseguimos dar manutenção na pipeline de diversos projetos alterando apenas em um local, facilitando na escalabilidade dos novos projetos.



